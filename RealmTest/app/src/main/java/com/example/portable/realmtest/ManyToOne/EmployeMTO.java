package com.example.portable.realmtest.ManyToOne;

import com.example.portable.realmtest.OneToOne.*;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Portable on 2016-11-22.
 */

public class EmployeMTO extends RealmObject{

    private int idEmploye;
    private String nomEmploye;

    public int getIdEmploye() {
        return idEmploye;
    }
    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }
    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }
}
