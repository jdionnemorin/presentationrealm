package com.example.portable.realmtest.OneToManyRecursif;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Portable on 2016-11-22.
 */

public class EmployeRefactor extends RealmObject {

    private static int ID_EMPLOYE = 0;

    @PrimaryKey
    private int idEmploye;

    private String nomEmploye;
    private RealmList<EmployeRefactor> sousEmploye;

    public EmployeRefactor() {
        this.idEmploye = ID_EMPLOYE++;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }
    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }
    public RealmList<EmployeRefactor> getSousEmploye() {
        return sousEmploye;
    }
    public void setSousEmploye(RealmList<EmployeRefactor> sousEmploye) {
        this.sousEmploye = sousEmploye;
    }
}
