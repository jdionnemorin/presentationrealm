package com.example.portable.realmconnextion;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Portable on 2016-11-29.
 */

public class Etudiant extends RealmObject{
    @PrimaryKey
    private int idEtudiant;

    private String nom;

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
