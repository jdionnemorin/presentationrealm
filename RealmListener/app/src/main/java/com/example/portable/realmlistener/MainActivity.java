package com.example.portable.realmlistener;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private Realm superRealm;
    private RealmChangeListener listenerRequete;
    private RealmChangeListener listenerResult;

    private RealmResults<User> utilisateurs;
    private User utilisateur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        superRealm = Realm.getDefaultInstance();

        listenerRequete = new RealmChangeListener() {
            public void onChange(RealmResults<User> utilisateur) {
                Toast.makeText(getApplicationContext(), "Requete", Toast.LENGTH_SHORT).show();
            }};

        // Find all the puppies
        utilisateurs = superRealm.where(User.class).lessThanOrEqualTo("age", 2).findAll();
        utilisateurs.addChangeListener(listenerRequete);

        listenerResult = new RealmChangeListener() {
            public void onChange(User utilisateur) {
                // ... do something with the updated Dog instance
            }};

        utilisateur = superRealm.where(User.class).equalTo("name", "David").findFirst();
        utilisateur.addChangeListener(listenerResult);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Remove the listeners
        superRealm.removeAllChangeListeners();
        // Close the Realm instance.
        superRealm.close();
    }
}
