package com.example.portable.realmtest.Jason;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Portable on 2016-11-29.
 */

public class ExempleJason extends RealmObject{

    @PrimaryKey
    private int id;

    private String patate;
    //private Context context;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getPatate() {
        return patate;
    }
    public void setPatate(String patate) {
        this.patate = patate;
    }

    public void creerObjetDeJason(String pathDuFichier){

        final String path = pathDuFichier;
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
        Realm superRealm = Realm.getDefaultInstance();

        superRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    InputStream stream = new FileInputStream(new File(path));
                    realm.createAllFromJson(ExempleJason.class, stream);
                } catch (IOException e) {
                }
            }
        });
    }
}
