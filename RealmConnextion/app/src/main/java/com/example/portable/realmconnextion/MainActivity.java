package com.example.portable.realmconnextion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.log.RealmLog;

public class MainActivity extends AppCompatActivity {

    private Realm superRealm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
        superRealm = Realm.getDefaultInstance();

        User user = User.login(Credentials.google("google token"),
                "http://realm.example.co:9080/auth");
        // Opening a remote Realm
        String realmURL = "realm://realm.example.com:9080/~/userRealm";
        SyncConfiguration configuration = new SyncConfiguration.Builder(user,
                realmURL).build();
        Realm realm = Realm.getInstance(syncConfiguration);
        // Enable full log output when debugging
        if (BuildConfig.DEBUG) {
            RealmLog.setLevel(Log.VERBOSE);
        }
    }
}
