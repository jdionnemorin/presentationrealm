package com.example.portable.realmtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.portable.realmtest.ManyToMany.DossierEmployeMTM;
import com.example.portable.realmtest.ManyToMany.EmployeMTM;
import com.example.portable.realmtest.OneToManyRecursif.EmployeRefactor;
import com.example.portable.realmtest.OneToOne.DossierEmploye;
import com.example.portable.realmtest.OneToOne.Employe;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Realm superRealm;
    private RealmChangeListener realmListener;
    private Employe employeOneToOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Configuration de la Base de Donnée Realm
         */
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
        superRealm = Realm.getDefaultInstance();

        /**
         * Lorsqu'il y a un changement dans la BD, du code est effectué
         */
        realmListener = new RealmChangeListener() {
            @Override
            public void onChange(Object element) {
                Toast.makeText(getApplicationContext(), "Changement Fait", Toast.LENGTH_SHORT).show();
            }
        };
        superRealm.addChangeListener(realmListener);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickOneToOneSave(view);
            }
        });
        Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickOneToOneLoad(view);
            }
        });

        Button button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickManyToManySave(view);
            }
        });
        Button button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickManyToManyLoad(view);
            }
        });

        Button button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { onClickTestEmployeSousEmploye(view); } });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /**
         * S'assurer d'enlever les Listeners
         */
        superRealm.removeChangeListener(realmListener);
        /**
         * Sinon
         */
        superRealm.removeAllChangeListeners();
        /**
         * S'assurer de fermer la BD quand on ferme l'application
         */
        superRealm.close();
    }

    public void onClickOneToOneSave(View v) {
        /**
         * Première manière d'exécuter une transaction avec Realm
         */
        superRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DossierEmploye dossierEmploye = new DossierEmploye();
                dossierEmploye.setIdDossier(1);
                dossierEmploye.setVille("Montreal");
                dossierEmploye.setProvince("Quebec");

                Employe employe = new Employe();
                employe.setIdEmploye(1);
                employe.setNomEmploye("Jonathan");
                employe.setDossierEmploye(dossierEmploye);
                try{
                    /**
                     * Sauvegarde ou met à jour les objets (Insert or Replace de SQLite)
                     */
                    superRealm.copyToRealmOrUpdate(dossierEmploye);
                    superRealm.copyToRealmOrUpdate(employe);
                }catch (Exception e){
                    Log.d(TAG, e.getMessage());
                }
            }
        });
    }

    public void onClickOneToOneLoad(View v){
        /**
         * Création d'une requête (SELECT * FROM employe.class)
         */
        RealmQuery<Employe> requete = superRealm.where(Employe.class);
        /**
         * Ajout des conditions (WHERE nomEmploye = Jonathan)
         */
        requete.equalTo("nomEmploye", "Jonathan");
        /**
         * Exécution de la requête
         * Un ou Plusieur, Trié ou non
         */
        RealmResults<Employe> resultat = requete.findAll();
        /**
         * Vérifie que la requête a été exécuté
         */
        if(resultat.isLoaded()){
            for(int i=0; i<resultat.size(); i++){
                Log.d(TAG, resultat.get(i).getNomEmploye() + " : " + resultat.get(i).getDossierEmploye().getProvince() + " , " + resultat.get(i).getDossierEmploye().getVille());
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_LONG).show();
        }
    }

    public void onClickManyToManySave(View v){
        DossierEmployeMTM dossier1 = new DossierEmployeMTM();
        dossier1.setIdDossier(1);
        dossier1.setProvince("Alberta");
        dossier1.setVille("Calgary");

        DossierEmployeMTM dossier2 = new DossierEmployeMTM();
        dossier2.setIdDossier(2);
        dossier2.setProvince("Colombie Britanique");
        dossier2.setVille("Vancouver");

        DossierEmployeMTM dossier3 = new DossierEmployeMTM();
        dossier3.setIdDossier(3);
        dossier3.setProvince("Manitoba");
        dossier3.setVille("Winnipeg");

        EmployeMTM employe1 = new EmployeMTM();
        employe1.setIdEmploye(1);
        employe1.setNomEmploye("David1");

        EmployeMTM employe2 = new EmployeMTM();
        employe2.setIdEmploye(2);
        employe2.setNomEmploye("David2");

        EmployeMTM employe3 = new EmployeMTM();
        employe3.setIdEmploye(3);
        employe3.setNomEmploye("David3");

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);
        /**
         * Deuxième manière d'exécuter une transaction
         */
        //Commence la transaction
        superRealm.beginTransaction();
        try {
            /**
             * Sauvegarde ou met à jour les objets
             */
            dossier1 = superRealm.copyToRealmOrUpdate(dossier1);
            dossier2 = superRealm.copyToRealmOrUpdate(dossier2);
            dossier3 = superRealm.copyToRealmOrUpdate(dossier3);
            employe1 = superRealm.copyToRealmOrUpdate(employe1);
            employe2 = superRealm.copyToRealmOrUpdate(employe2);
            employe3 = superRealm.copyToRealmOrUpdate(employe3);
            //Rajoute des dossiers aux employés
            employe1.getListeDossierEmploye().add(dossier2);
            employe1.getListeDossierEmploye().add(dossier3);
            employe2.getListeDossierEmploye().add(dossier3);
            employe3.getListeDossierEmploye().add(dossier1);
            //Rajoute des employés aux dossiers
            dossier1.getListeEmploye().add(employe1);
            dossier1.getListeEmploye().add(employe2);
            dossier1.getListeEmploye().add(employe3);
            /**
             * Sauvegarde (commit) la transaction
             */
            superRealm.commitTransaction();
            Log.d(TAG, "Commit");
        }catch (Exception e){
            /**
             * Rollback
             */
            superRealm.cancelTransaction();
        }
    }

    public void onClickManyToManyLoad(View v){
        RealmQuery<EmployeMTM> requeteEmpMtM = superRealm.where(EmployeMTM.class);
        RealmResults<EmployeMTM> resultEmtm = requeteEmpMtM.findAll();
        RealmQuery<DossierEmployeMTM> requeteDospMtM = superRealm.where(DossierEmployeMTM.class);
        RealmResults<DossierEmployeMTM> resultDmtm = requeteDospMtM.findAll();

        if(resultEmtm.isLoaded() && resultDmtm.isLoaded()){
            for(int i=0; i<resultEmtm.size(); i++){
                Log.d(TAG, resultEmtm.get(i).getIdEmploye()+","+resultEmtm.get(i).getNomEmploye()+"|"+resultEmtm.get(i).getListeDossierEmploye().toString());
            }
            for(int i=0; i<resultDmtm.size(); i++) {
                Log.d(TAG, resultDmtm.get(i).getIdDossier() + "," + resultDmtm.get(i).getProvince() + "," + resultDmtm.get(i).getVille() + "|" + resultDmtm.get(i).getListeEmploye().toString());
            }

            Log.d(TAG, "Load ManyToMany");
        }
    }

    public void onClickTestEmployeSousEmploye(View v){
        EmployeRefactor employe1 = new EmployeRefactor();
        employe1.setNomEmploye("David1");
        EmployeRefactor employe2 = new EmployeRefactor();
        employe2.setNomEmploye("David2");
        EmployeRefactor employe3 = new EmployeRefactor();
        employe3.setNomEmploye("David3");

        superRealm.beginTransaction();
        employe1 = superRealm.copyToRealmOrUpdate(employe1);
        employe2 = superRealm.copyToRealmOrUpdate(employe2);
        employe3 = superRealm.copyToRealmOrUpdate(employe3);

        employe1.getSousEmploye().add(employe2);
        employe1.getSousEmploye().add(employe3);
        superRealm.commitTransaction();

        RealmQuery<EmployeRefactor> requeteEmploye = superRealm.where(EmployeRefactor.class);
        RealmResults<EmployeRefactor> resultEmploye = requeteEmploye.findAll();

        if(resultEmploye.isLoaded()){
            for(int i=0; i<resultEmploye.size(); i++){
                Log.d(TAG, resultEmploye.get(i).getNomEmploye()+"|"+resultEmploye.get(i).getSousEmploye().toString());
            }
            Log.d(TAG, "Load OneToLuiMeme");
        }
    }
}
