package com.example.portable.realmtest.OneToOne;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Portable on 2016-11-22.
 */

public class Employe extends RealmObject{

    @PrimaryKey
    private int idEmploye;

    private String nomEmploye;

    //Lien avec la classe DossierEmploye, Chaque Employe possède un DossierEmploye
    private DossierEmploye dossierEmploye;


    public int getIdEmploye() {
        return idEmploye;
    }
    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }
    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }

    public DossierEmploye getDossierEmploye() {
        return dossierEmploye;
    }
    public void setDossierEmploye(DossierEmploye dossierEmploye) { this.dossierEmploye = dossierEmploye; }
}
