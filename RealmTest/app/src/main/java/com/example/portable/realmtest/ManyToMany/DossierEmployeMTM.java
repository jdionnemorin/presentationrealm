package com.example.portable.realmtest.ManyToMany;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Portable on 2016-11-22.
 */

public class DossierEmployeMTM extends RealmObject{

    @PrimaryKey
    private int idDossier;

    private String Ville;
    private String Province;

    //Lien Many-To-Many
    private RealmList<EmployeMTM> listeEmploye;

    public int getIdDossier() {
        return idDossier;
    }
    public void setIdDossier(int idDossier) {
        this.idDossier = idDossier;
    }

    public String getVille() {
        return Ville;
    }
    public void setVille(String ville) {
        Ville = ville;
    }

    public String getProvince() {
        return Province;
    }
    public void setProvince(String province) {
        Province = province;
    }

    public RealmList<EmployeMTM> getListeEmploye() { return listeEmploye; }
    public void setListeEmploye(RealmList<EmployeMTM> listeEmploye) { this.listeEmploye = listeEmploye; }
}
