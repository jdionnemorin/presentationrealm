package com.example.portable.realmcrud;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    private Realm realm;
    private EditText nomEdit;
    private EditText couleurEdit;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);

        realm = Realm.getDefaultInstance();

        nomEdit = (EditText) findViewById(R.id.nom);
        couleurEdit = (EditText) findViewById(R.id.couleur);
        text = (TextView) findViewById(R.id.textView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void onClickCreate(View view){
        if(!nomEdit.getText().toString().isEmpty() && !couleurEdit.getText().toString().isEmpty()){

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Animal animal = realm.createObject(Animal.class);
                    animal.setCouleur(couleurEdit.getText().toString());
                    animal.setNom(nomEdit.getText().toString());

                    Toast.makeText(getApplicationContext(), "L'animal a été enregistrer avec succès", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void onClickFind(View view){
            RealmQuery<Animal> requete = realm.where(Animal.class);
            RealmResults<Animal> resultat = requete.findAll();

            if(resultat.isLoaded() && resultat.size() != 0){
                String listeAnimaux = "";
                for(Animal animal : resultat){
                    listeAnimaux += "nom : " + animal.getNom() + ", couleur : " + animal.getCouleur() + "\n";
                }
                text.setText(listeAnimaux);
            }

    }

    public void onCliclDelete(View view){
        if(!nomEdit.getText().toString().isEmpty() && !couleurEdit.getText().toString().isEmpty()){
            RealmQuery<Animal> requete = realm.where(Animal.class);
            requete.equalTo("nom",nomEdit.getText().toString()).equalTo("couleur",couleurEdit.getText().toString());

            final RealmResults<Animal> resultat = requete.findAll();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Animal animal = resultat.first();
                    animal.deleteFromRealm();
                    Toast.makeText(getApplicationContext(), "L'animal a été supprimé avec succès", Toast.LENGTH_LONG).show();
                }
            });

        }
    }




}
