package com.example.portable.realmtest.ManyToMany;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Portable on 2016-11-22.
 */

public class EmployeMTM extends RealmObject{

    @PrimaryKey
    private int idEmploye;

    private String nomEmploye;

    //Lien Many-To-Many. Peut aussi servir à faire un lien Many-To-One
    private RealmList<DossierEmployeMTM> listeDossierEmploye;

    public int getIdEmploye() {
        return idEmploye;
    }
    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }
    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }

    public RealmList<DossierEmployeMTM> getListeDossierEmploye() { return listeDossierEmploye; }
    public void setListeDossierEmploye(RealmList<DossierEmployeMTM> listeDossierEmploye) { this.listeDossierEmploye = listeDossierEmploye; }
}
