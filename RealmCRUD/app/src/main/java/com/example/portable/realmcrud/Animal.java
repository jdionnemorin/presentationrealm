package com.example.portable.realmcrud;

import io.realm.RealmObject;

/**
 * Created by portable on 16-11-22.
 */

public class Animal extends RealmObject {

    private String nom;
    private String couleur;


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
}
