package com.example.portable.realmtest.ManyToOne;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Portable on 2016-11-22.
 */

public class DossierEmployeMTO extends RealmObject{

    private int    idDossier;
    private String Ville;
    private String Province;

    //Chaque dossier peut contenir plusieurs employe
    private RealmList<EmployeMTO> employe;

    public int getIdDossier() {
        return idDossier;
    }
    public void setIdDossier(int idDossier) {
        this.idDossier = idDossier;
    }

    public String getVille() {
        return Ville;
    }
    public void setVille(String ville) {
        Ville = ville;
    }

    public String getProvince() {
        return Province;
    }
    public void setProvince(String province) {
        Province = province;
    }

    public RealmList<EmployeMTO> getEmploye() {
        return employe;
    }
    public void setEmploye(RealmList<EmployeMTO> employe) {
        this.employe = employe;
    }
}
